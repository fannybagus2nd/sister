<?php
    include 'koneksi.php';
    $db = new database();
    $db1 = new database();
    
        $no=1;
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>	
    
	<title>Sistem Terdistribusi</title>
</head>
<body>
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">
        <img src="Tidak-Tahu.png" width="30" height="30" class="d-inline-block align-top" alt="">
        Sistem Terdistribusi
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item ">
            <a class="nav-link" href="index.php">Home </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="hero.php">Hero<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="akun.php">Akun</a>
          </li>
        </ul>
      </div>
    </nav>
    </div>
    <div class="container">
        <h4 class="mt-3 mb-3">Tambah Skin</h4> 
        <?php foreach ($db->ambilid($_GET['heroid'])as $mem) : ?>
        <form action="proses.php?aksi=s_insert" method="post">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
            <div class="form-group">
                <label for="id_hero">ID Hero</label>
                <input type="text" placeholder="" id="id_hero" name="id_hero" readonly class="form-control" value="<?= $mem['HEROID'] ?>" >  
            </div>
            <div class="form-group">
                <label for="id_skin">ID Skin</label>
                <input type="text" placeholder="" id="id_skin" name="id_skin"  class="form-control" >  
            </div>
            <div class="form-group">
                <label for="nama_skin">Nama Skin</label>
                <input type="text" placeholder="" id="nama_skin" name="nama_skin"  class="form-control" >  
            </div>
            <div class="form-group">
                <label for="harga_skin">Harga Skin</label>
                <input type="number" placeholder="" id="harga_skin" name="harga_skin"  class="form-control" " >  
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
    </form>
            <?php endforeach ?>
    
     </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script language="javascript">
        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
    </script>
</body>
</html>
