<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "sistemterdistribusi",
        $con;
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }


        public function tampildata(){
            $sql = "SELECT h.heroid, h.heronama, h.heroharga
                    FROM hero h";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function tampilrole($hero_id){         // fungsi mengambil data
            $sql = "SELECT h.`HEROID`, h.`HERONAMA`, r.`ROLENAMA`, r.`ROLEID`
                    FROM HERO h,PUNYAROLE p, ROLE r
                    WHERE h.`HEROID` = p.`HEROID` AND r.`ROLEID`= p.`ROLEID` AND h.`HEROID` = '$hero_id'";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }
        public function tampilskin($hero_id){         // fungsi mengambil data
            $sql = "SELECT HERO.`HEROID`,HERO.`HERONAMA`,HERO.`HEROID`, SKIN.`SKINID`, SKIN.`SKINNAMA`, SKIN.`SKINHARGA`
                    FROM HERO ,SKIN
                    WHERE HERO.HEROID = SKIN.`HEROID` AND  HERO.`HEROID` = '$hero_id'";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

         public function tampildataakun(){
            $sql = "SELECT akunid,akunnama,akunsaldo FROM akun";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function daftarskin($akunid){         // fungsi mengambil data
            $sql = "SELECT skin.skinid, skin.skinnama, skin.skinharga
                    FROM akun, skin, punyaskin
                    WHERE punyaskin.skinid=skin.skinid AND punyaskin.akunid=akun.akunid AND akun.akunid='$akunid'";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }
        public function daftarhero($akunid){         // fungsi mengambil data
            $sql = "SELECT hero.heroid, hero.heronama, hero.heroharga
                    FROM akun, hero, punyahero
                    WHERE punyahero.heroid=hero.heroid AND punyahero.akunid=akun.akunid AND akun.akunid='$akunid' ";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

         public function ambilrole(){
            $sql = "SELECT * FROM role";
            $datrole = mysqli_query($this->con,$sql);
            while ($dg = mysqli_fetch_array($datrole))
            {
                $hasilg[] = $dg;
            }
            return $hasilg;
        }
         public function insertrole($heroid,$role_id){
            $judul=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into punyarole(HEROID, ROLEID) values(
                '$heroid','$role_id')";
            $result=mysqli_query($this->con,$sql);
            $hasil['respon']="gagal";
            return $hasil;
        }

        public function ambilid($hero_id){         // fungsi mengambil data
            $sql = "SELECT HEROID
                    FROM hero
                    WHERE HEROID = '$hero_id'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }
        public function deleterole($heroid,$roleid){
            $sql = "DELETE from punyarole where HEROID = '$heroid' AND ROLEID ='$roleid'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                $hasil['respon']="sukses";
                return $hasil; //delete data sukses
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        
        }
        public function insertskin($id_hero,$id_skin,$nama_skin,$harga_skin){
            $judul=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into skin(HEROID, SKINID, SKINNAMA, SKINHARGA) values(
                '$id_hero','$id_skin','$nama_skin','$harga_skin')";
            $result=mysqli_query($this->con,$sql);
            $hasil['respon']="gagal";
            return $hasil;
        }
        public function deleteskin($skinid){
            $sql = "DELETE from skin where SKINID = '$skinid'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                $hasil['respon']="sukses";
                return $hasil; //delete data sukses
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        
        }

    }
?>